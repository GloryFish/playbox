#!/usr/local/bin/python

import random
import pygame
from pygame.locals import *
import boxes
from palettes import *
from PyTweener import Tweener
import cProfile
import argparse
import pstats
import importlib
import jukebox
import time

def main(args):

  def random_playbox():
    if args.box is not None:
      return getattr(boxes, args.box)
    return random.choice([
      boxes.PlayboxAnimals,
      boxes.PlayboxRobot,
      boxes.PlayboxNumbers,
      boxes.PlayboxLetters,
    ])

  pygame.init()

  pygame.mouse.set_visible(False)

  font = pygame.font.Font(None, 18)
  titleFont = pygame.font.Font(None, 300)
  palette = random_palette()

 # Initialize Tweening engine
  tweener = Tweener()

  # Initialize clock
  clock = pygame.time.Clock()

  # Initialise screen
  if args.fullscreen:
    screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
  else:
    screen = pygame.display.set_mode((1280, 720))

  pygame.display.set_caption('Playbox')

  # Fill canvas
  canvas = pygame.Surface(screen.get_size())
  canvas = canvas.convert()
  canvas.fill(palette[0])

  # Blit everything to the screen
  screen.blit(canvas, (0, 0))
  pygame.display.flip()

  def add_playbox():
    newPlaybox = random_playbox()
    playboxes.append(newPlaybox(canvas, palette, tweener))

  # Initialize Playboxes
  playboxes = []
  add_playbox()

  jb = jukebox.Jukebox()

  # Start music
  if not args.nomusic:
    pygame.mixer.music.set_endevent(jukebox.MUSIC_STOPPED)
    jb.play()

  quit_combo_time = time.time()
  quit_combo_duration = 5

  # Event loop
  while 1:
    # Handle events
    for event in pygame.event.get():
      if event.type == QUIT:
        return

      if event.type == pygame.KEYDOWN:
        # Handle QUITing
        keys = pygame.key.get_pressed()

        # With easyquit we can exit just by pressing space
        if args.easyquit and keys[K_ESCAPE]:
          return

        # Otheriwse if we start holding down both ESCAPE and SPACE, we keep track of the time.
        # On KEYUP, we'll check to see how long they were held for
        elif event.key in [K_ESCAPE, K_SPACE] and keys[K_ESCAPE] and keys[K_SPACE]:
          quit_combo_time = time.time()

      if event.type == pygame.KEYUP:
        if not args.easyquit and event.key in [K_ESCAPE, K_SPACE]:
          if time.time() - quit_combo_time >= quit_combo_duration:
            return

        [playbox.keypressed(event.key) for playbox in playboxes if not playbox.isFinished()]

      if event.type == jukebox.MUSIC_STOPPED:
        jb.play()


    canvas.fill(palette[0])

    # Playbox title
    currentPlayboxText = titleFont.render(", ".join([str(playbox) for playbox in playboxes if not playbox.isFinished()]), 1, palette[1])
    currentPlayboxTextPos = currentPlayboxText.get_rect()
    currentPlayboxTextPos.centerx = canvas.get_rect().centerx
    currentPlayboxTextPos.centery = 120
    canvas.blit(currentPlayboxText, currentPlayboxTextPos)

    # Calculate delta time
    dt = clock.get_time() / 1000.0

    # Update tweening engine
    tweener.update(dt)

    # Update and render playboxes
    for playbox in playboxes:
      playbox.update(dt)
      playbox.draw(canvas)

    # Remove finished Playboxes
    playboxes = [playbox for playbox in playboxes if not playbox.shouldBeDestroyed()]

    # Check if all Playboxes are finished, if so, add a new one
    if len([playbox for playbox in playboxes if not playbox.isFinished()]) == 0:
      add_playbox()
      palette = random_palette()

    # FPS Counter
    fpsText = font.render(str(clock.get_fps()), 1, (10, 10, 10))
    canvas.blit(fpsText, (20, 20))

    screen.blit(canvas, (0, 0))
    pygame.display.flip()

    clock.tick(30)

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Playbox')
  parser.add_argument('-p', '--profile', help='Profile the running application', action="store_true")
  parser.add_argument('-f', '--fullscreen', help='Run Playbox in fullscreen mode', action="store_true")
  parser.add_argument('-b', '--box', help='Run with only the specified box.')
  parser.add_argument('-n', '--nomusic', help='Don\'t play any music', action='store_true')
  parser.add_argument('-e', '--easyquit', help='Quit the game by pressing Escape. Otherwise, press and hold escape and space for several seconds', action='store_true')
  args = parser.parse_args()

  if args.profile:
    print('Profiling Playbox')
    cProfile.run('main(args)', '../profile.bin')
    stats = pstats.Stats('../profile.bin')
    stats.sort_stats('cumtime')
    stats.print_stats(20)

  else:
    main(args)