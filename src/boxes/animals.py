import pygame
import random
from collections import namedtuple
import os
from pictures import PlayboxPictures

class PlayboxAnimals(PlayboxPictures):
  """Displays animal pictures."""

  def __str__(self):
    return "Animals!"

  def __init__(self, canvas, palette, tweener):
    path = 'sprites/animals'
    PlayboxPictures.__init__(self, canvas, palette, tweener, path, 3)
