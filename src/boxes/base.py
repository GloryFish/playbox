import pygame

class Playbox():
  """Base class for Playbox modules."""

  def __init__(self, canvas, palette, tweener):
    self.startTime = pygame.time.get_ticks()
    self.palette = palette
    self.tweener = tweener
    self.canvas = canvas

    video_info = pygame.display.Info()
    self.bounds = (video_info.current_w, video_info.current_h)

  def __str__(self):
    return self.__class__.__name__

  def open(self):
    """Initalize the Playbox."""
    pass

  def keypressed(self, key):
    pass

  def update(self, dt):
    pass

  def draw(self):
    pass

  def isFinished(self):
    return True

  def shouldBeDestroyed(self):
    return False