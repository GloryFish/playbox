from base import Playbox
import pygame
import random
from PyTweener import *
from collections import namedtuple
from vector import Vector2

class Number():
  def __init__(self, digit, color, position):
    self.digit = digit
    self.color = color
    self.position = position

  def set_y(self, y):
    self.position.y = y

  def get_y(self):
    return self.position.y

class PlayboxNumbers(Playbox):

  def __str__(self):
    return "Numbers!"

  def __init__(self, canvas, palette, tweener):
    Playbox.__init__(self, canvas, palette, tweener)
    self.font = pygame.font.Font(None, 288)
    self.numbers = []
    self.max_numbers = 10

  def keypressed(self, key):
    if self.isFinished():
      return

    newNumber = Number(
      str(len(self.numbers) + 1),
      random.choice(self.palette[1:]),
      Vector2(random.randint(100, self.bounds[0] - 100), random.randint(100, self.bounds[1] - 100))
      )
    self.numbers.append(newNumber)

    if self.isFinished():
      for number in self.numbers:
        target = number.position.y + self.bounds[1]
        self.tweener.add_tween(number.position, y=target, tween_time=2.0, tween_type=self.tweener.IN_QUAD)

  def draw(self, canvas):
    for index, number in enumerate(self.numbers):
      canvas.blit(self.font.render(number.digit, 1, (0, 0, 0)), (number.position.x + 1, number.position.y + 1))
      canvas.blit(self.font.render(number.digit, 1, number.color), number.position)

  def isFinished(self):
    return len(self.numbers) >= self.max_numbers

  def shouldBeDestroyed(self):
    # Destroy this if there are no more numbers below the bottom of the screen
    return self.isFinished() and len([number for number in self.numbers if number.position.y < self.bounds[1]]) == 0
