from base import Playbox
import pygame
import random

class PlayboxRobot(Playbox):

  def __str__(self):
    return "Robot!"

  def __init__(self, canvas, palette, tweener):
    Playbox.__init__(self, canvas, palette, tweener)

    self.animations = dict()
    self.animations["walk"] = []
    for index in range(1, 5):
      image = pygame.transform.scale2x(pygame.image.load("sprites/robot_walk_%d.png" % index).convert())
      colorkey = image.get_at((0,0))
      image.set_colorkey(colorkey, pygame.RLEACCEL)
      self.animations["walk"].append(image)

    self.animations["jump"] = []
    image = pygame.transform.scale2x(pygame.image.load("sprites/robot_jump_1.png").convert())
    colorkey = image.get_at((0,0))
    image.set_colorkey(colorkey, pygame.RLEACCEL)
    self.animations["jump"].append(image)

    self.animations["fall"] = []
    image = pygame.transform.scale2x(pygame.image.load("sprites/robot_jump_2.png").convert())
    colorkey = image.get_at((0,0))
    image.set_colorkey(colorkey, pygame.RLEACCEL)
    self.animations["fall"].append(image)

    self.animationElapsed = 0
    self.animationDuration = 0.25
    self.animationFrame = 0
    self.state = "walk"

    self.jumpheight = 0.0
    self.jumpvelocity = 0.0
    self.gravity = 1000

    if random.choice(["left", "right"]) == "right":
      self.position = { "x": -200, "y": random.randint(200, self.bounds[1] - 200) }
      self.movement = 150
      self.flipped = False
    else:
      self.position = { "x": self.bounds[1] + 200, "y": random.randint(200, self.bounds[1] - 200) }
      self.movement = -150
      self.flipped = True


  def keypressed(self, key):
    if self.state == "walk":
      self.state = "jump"
      self.animationElapsed = 0
      self.animationFrame = 0
      self.jumpvelocity = 500.0
    pass

  def update(self, dt):
    self.animationElapsed += dt

    if self.state == "jump":
      self.jumpheight += self.jumpvelocity * dt
      self.jumpvelocity -= self.gravity * dt
      if self.jumpvelocity < 0:
        self.state = "fall"

    elif self.state == "fall":
      self.jumpheight += self.jumpvelocity * dt
      self.jumpvelocity -= self.gravity * dt
      if self.jumpheight < 0:
        self.jumpheight = 0
        self.state = "walk"

    elif self.state == "walk":
      if self.animationElapsed > self.animationDuration:
        self.animationElapsed = 0
        self.animationFrame += 1
        print("%d", self.animationFrame)
        if self.animationFrame == len(self.animations[self.state]):
          self.animationFrame = 0

    self.position["x"] += self.movement * dt

  def draw(self, canvas):
      image = self.animations[self.state][self.animationFrame]
      if self.flipped:
        image = pygame.transform.flip(image, True, False)
      canvas.blit(image, (self.position["x"], self.position["y"] - self.jumpheight))


  def isFinished(self):
    #  Robot is off screen
    return self.position["x"] > self.bounds[0] + 210 or self.position["x"] < -210

  def shouldBeDestroyed(self):
    #  Robot is off screen
    return self.position["x"] > self.bounds[0] + 210 or self.position["x"] < -210
