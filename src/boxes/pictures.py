import pygame
import random
from collections import namedtuple
import os
from base import Playbox
from vector import Vector2

Picture = namedtuple("Picture", "surface rect")

class PlayboxPictures(Playbox):
  """Displays pictures from a folder."""

  def __str__(self):
    return "Pictures!"

  def __init__(self, canvas, palette, tweener, path, display_count):
    Playbox.__init__(self, canvas, palette, tweener)

    self.font = pygame.font.Font(None, 100)

    # Enumerate all subfolders and select some randomly
    DIRNAMES = 1
    all_folders = os.walk(path).next()[DIRNAMES]

    assert len(all_folders) >= display_count, "Must specify at least %d folders" % display_count

    self.folders = random.sample(all_folders, display_count)
    self.surfaces = dict()

    # Choose up to 3 random pictures from each folder and create surfaces from them
    for folder in self.folders:
      self.surfaces[folder] = []

      all_pics_in_folder = [filename for filename in os.listdir("%s/%s" % (path, folder)) if filename.endswith(".jpg")]

      for filename in random.sample(all_pics_in_folder, min(len(all_pics_in_folder), 3)):
        surface = self.surface_for_image_path("%s/%s/%s" % (path, folder, filename))

        canvaspos = self.canvas.get_rect()
        picpos = surface.get_rect()
        picpos.centerx = canvaspos.centerx - 300
        picpos.centery = 0 - surface.get_height()

        picture = Picture(surface, picpos)
        self.surfaces[folder].append(picture)

    self.show_next()

  def show_next(self):
    # Increment the current_index
    try:
      self.current_index += 1

      # Tween the previous image out
      self.tweener.add_tween(self.surfaces[self.folders[0]][self.current_index - 1].rect, y=self.canvas.get_height() + 100, tween_time=0.5, tween_type=self.tweener.IN_BACK)

    except AttributeError:
      self.current_index = 0

    # If we've shown all of the pictures in the current folder, remove it
    if self.current_index == len(self.surfaces[self.folders[0]]):
      self.folders.pop(0)
      self.current_index = 0

    # If we are all out of folders, then we are done
    if len(self.folders) == 0:
      return

    # Tween the current image in
    canvaspos = self.canvas.get_rect()
    target = canvaspos.centery

    self.tweener.add_tween(self.surfaces[self.folders[0]][self.current_index].rect, centery=target, tween_time=1.0, tween_type=self.tweener.OUT_BOUNCE)


  def surface_for_image_path(self, image_path):
    image = pygame.image.load(image_path).convert()
    frame = pygame.Surface((image.get_width() + 10, image.get_height() + 10))
    frame.fill(random.choice(self.palette[1:]))
    imagepos = image.get_rect()
    imagepos.centerx = frame.get_rect().centerx
    imagepos.centery = frame.get_rect().centery
    frame.blit(image, imagepos)
    return frame

  def add_picture(self):
    folder = random.choice(self.surfaces)
    surfaces = []
    pic = Picture(folder, surfaces, 100, 100)



  def keypressed(self, key):
    if self.isFinished():
      return

    self.show_next()

  def draw(self, canvas):
    if self.isFinished():
      return

    canvaspos = canvas.get_rect()
    for pic in self.surfaces[self.folders[0]]:
      canvas.blit(pic.surface, pic.rect)

    canvas.blit(self.font.render(self.folders[0], 1, self.palette[2]), (canvaspos.centerx + 10, canvaspos.centery - 30))

  def isFinished(self):
    return len(self.folders) == 0

  def shouldBeDestroyed(self):
    # Destroy this if there are no more numbers below the bottom of the screen
    return False
