from base import Playbox
import pygame
import random
import string

class PlayboxLetters(Playbox):

  def __str__(self):
    return "Letters!"

  def __init__(self, canvas, palette, tweener):
    Playbox.__init__(self, canvas, palette, tweener)
    self.font = pygame.font.Font(None, 288)
    self.letters = []
    self.palette = palette

  def keypressed(self, key):
    newLetter = {
      'character': string.ascii_uppercase[len(self.letters)],
      'color': random.choice(self.palette[1:]),
      'position': (random.randint(100, self.bounds[0] - 100), random.randint(100, self.bounds[1] - 100))
    }
    self.letters.append(newLetter)

  def draw(self, canvas):
    for index, letter in enumerate(self.letters):
      canvas.blit(self.font.render(letter['character'], 1, (0, 0, 0)), (letter['position'][0] + 1, letter['position'][1] + 1))
      canvas.blit(self.font.render(letter['character'], 1, letter['color']), letter['position'])

  def isFinished(self):
    return len(self.letters) > 10

  def shouldBeDestroyed(self):
    return len(self.letters) > 10