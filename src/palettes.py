from pygame import Color
import random

palettes = [
  [
    Color("#0E8F7B"),
    Color("#E9D392"),
    Color("#F4E6BB"),
    Color("#EABCF4"),
    Color("#E07EF4"),
  ],
  [
    Color("#D76C57"),
    Color("#FFC6BA"),
    Color("#808F73"),
    Color("#51655F"),
    Color("#98BEB3"),
  ],
  [
    Color("#98C455"),
    Color("#FFC632"),
    Color("#EE641C"),
    Color("#ED0457"),
    Color("#B43B5E"),
  ],
  [
    Color("#38638D"),
    Color("#4F81BC"),
    Color("#F19349"),
    Color("#EFEFEF"),
    Color("#333333"),
  ],
  [
    Color("#85F92C"),
    Color("#F9E72C"),
    Color("#F92C66"),
    Color("#2CC4F9"),
    Color("#2C2FF9"),
  ],
]

def random_palette():
  palette = random.choice(palettes)
  random.shuffle(palette)
  return palette

