import pygame
import os
import random

MUSIC_STOPPED = pygame.USEREVENT + 1

class Jukebox():

  def __init__(self):
    # Get list of music files
    # Enumerate all subfolders and select some randomly
    DIRNAMES = 1
    self.songs = [item for item in os.listdir('music') if item.endswith(('.ogg', '.mod', '.OGG', '.MOD'))]

  def play(self):
    # select a random song and start playing it
    song = random.choice(self.songs)
    print('Playing %s' % song)
    pygame.mixer.music.load('music/%s' % song)
    pygame.mixer.music.set_volume(0.5)
    pygame.mixer.music.play()