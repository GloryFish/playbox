Create folders here with the animal name. Folders should then contain one or more numbered images with pictures of that animal.

animals
  - Marmot
    - 0.jpg
    - 1.jpg
    - 2.jpg
  - Antelope
    - 0.jpg

And so on...