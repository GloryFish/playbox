#! /usr/local/bin/python

"""

Fetch Creative Commons animal photos from: http://animalphotos.info/a/

"""

from bs4 import BeautifulSoup
import urllib2
import os
import time

path = "../src/sprites/animals"

if not os.path.isdir(path):
  print("%s does not exist" % path)
  exit()

response = urllib2.urlopen("http://animalphotos.info/a/")
main_html = response.read()

main_soup = BeautifulSoup(main_html)

#  Get list of links to animal pages
anchors = main_soup.select(".cat-item a")

animal_index = []

for anchor in anchors:
  animal_index.append((anchor.string, anchor.get("href")))

print(str(len(animal_index)))

for item in animal_index[232:]:
  print "Fetching %s pics from %s..." % item

  response = urllib2.urlopen(item[1])
  html = response.read()
  soup = BeautifulSoup(html)

  images = soup.select(".entry img")

  if len(images) > 0:
    folder = "%s/%s" % (path, item[0])
    if not os.path.isdir(folder):
      os.mkdir(folder)

    for index, image in enumerate(images):
      url = image.get("src")

      if url is not None and url:
        print(url)

        file_path, file_extension = os.path.splitext(url)

        image_file = urllib2.urlopen(url)

        destination_filename = "%s/%d%s" % (folder, index, file_extension)

        if not os.path.exists(destination_filename):
          with open(destination_filename,'wb') as output:
            output.write(image_file.read())
        else:
          print("...skipped")

  print "Downloaded %d images" % len(images)
  print "Waiting for two minutes..."
  time.sleep(60)
  print "One minute remaining..."
  time.sleep(30)
  print "30 seconds remaining..."
  time.sleep(30)
